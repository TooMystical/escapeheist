using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeDoorStor : MonoBehaviour
{
    private bool pressed = false;
    private Animator animator;
    public void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    public void OnPress()
    {

            animator.SetBool("CodeCorrect", true);
            pressed = true;
  
    }

}

