using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
        private bool pressed = false;
        private Animator animator;
        public void Start()
        {
            animator = GetComponent<Animator>();
        }

        // Start is called before the first frame update
        public void OnPress()
        {
            Debug.Log("button down");


            if (!pressed)
            {
                animator.SetBool("KeyIn", true);
                pressed = true;
            }
        }

    }

