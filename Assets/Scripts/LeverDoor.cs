using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LeverDoor : MonoBehaviour
{

    public GameObject door;
    public UnityEvent Open, Closed;

    private bool isOpen = false;
    private bool isTriggered = false;



    private void OnTriggerEnter(Collider other)
    {

        if (isOpen == false && other.CompareTag("open"))
        {
            OpenDoor();

            
        }

        if (isOpen == true && other.CompareTag("close"))
        {
            CloseDoor();
            
        }


        isTriggered = true;

    }
    private void OnTriggerExit(Collider other)
    {
        isTriggered = false;
    }
    private void OpenDoor()
    {
        isOpen = true;
        Open.Invoke();
        Debug.Log("Open");
    }
    private void CloseDoor()
    {
        isOpen = false;
        Closed.Invoke();
        Debug.Log("Closed");
    }
}

