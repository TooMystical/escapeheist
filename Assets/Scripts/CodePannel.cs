using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;
using UnityEngine.Events;

public class CodePannel : MonoBehaviour
{
    // Start is called before the first frame update

    
    public GameObject codeText;
    private string codeTextValue = "";
    public string code;
    public UnityEvent codeCorrect;

    // Update is called once per frame

    private void Start()
    {
        
    }

    private void Update()
    {


        codeText.GetComponent<TextMesh>().text = codeTextValue;
        if (codeTextValue.Length >= code.Length)
        { 
            CheckCode();
        }

    }


    public void CheckCode()
    {
        if (codeTextValue == code)
        {
            CodeCorrect();
        }
        else
        {
            codeTextValue = "";
        }

    }

    private void CodeCorrect()
    {

        codeCorrect.Invoke();
        codeTextValue = "code correct!";
        
    }

        public void AddDigit(string digit)
    {

        codeTextValue += digit;
    }

}
