using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChangeOne : MonoBehaviour
{
    public string scenename;

    private void OnCollisionEnter(Collision collision)
    {
        SceneManager.LoadScene(scenename);
    }
}
