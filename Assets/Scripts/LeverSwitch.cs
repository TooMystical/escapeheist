using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverSwitch : MonoBehaviour
{
    public void OnOpen()
    {
        gameObject.SetActive(false);
    }
    public void OnClose()
    {
        gameObject.SetActive(true);
    }
}
