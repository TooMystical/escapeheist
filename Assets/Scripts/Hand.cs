using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Hand : MonoBehaviour
{
    // Start is called before the first frame update

    Animator animator;
    SkinnedMeshRenderer mesh;
    private float _triggerTarget;
    private float _gripTarget;
    private float _gripCurrent;
    private float _triggerCurrent;
    private string _animatorGripPeram = "Grip";
    private string _animatorTriggerPeram = "Trigger";
    

    public float speed;
    void Start()
    {
        
        animator = GetComponent<Animator>();
        mesh = GetComponentInChildren<SkinnedMeshRenderer>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        AnimateHand();
    }

    internal void SetTrigger(float v)
    {
        _gripTarget = v;
    }

    internal void SetGrip(float v)
    {
        _triggerTarget = v;
    }


    void AnimateHand()
    {
        if (_gripCurrent != _gripTarget)
        {
            _gripCurrent = Mathf.MoveTowards(_gripCurrent, _gripTarget, Time.deltaTime * speed);
            
            animator.SetFloat(_animatorGripPeram, _gripCurrent);
        }

        if (_triggerCurrent != _triggerTarget)
        {
           _triggerCurrent = Mathf.MoveTowards(_triggerCurrent, _triggerTarget, Time.deltaTime * speed);

            animator.SetFloat(_animatorTriggerPeram, _triggerCurrent);
        }
    }
    
}
