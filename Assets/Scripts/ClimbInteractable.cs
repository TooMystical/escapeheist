using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ClimbInteractable : XRBaseInteractable
{
    
    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {

        
        XRBaseInteractor interactor = args.interactor;
        Climber.velocityHand = args.interactor.GetComponent<ControllerVelocity>();
        base.OnSelectEntered(args);
        Debug.Log(args.interactor);
        if (interactor is XRDirectInteractor)

        {
            Climber.controllerCount += 1;
            Climber.climbingHand = interactor.GetComponent<XRController>();
            Climber.isOn = true;
            Debug.Log("DIRECT INTERACTOR" + Climber.controllerCount);
        }
       
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        Climber.controllerCount -= 1;
        Debug.Log("DeSelected");
        XRBaseInteractor interactor = args.interactor;
        base.OnSelectExited(args);
        Climber.velocityHand = null;
        Climber.climbingHand = null;
        


    }

     
}