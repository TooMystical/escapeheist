using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;


public class Player_Controller : MonoBehaviour
{

    [SerializeField] private InputActionReference jumpActionRefernceRight;
    [SerializeField] private InputActionReference jumpActionRefernceLeft;

    [SerializeField] private float jumpForce = 500;
    private Vector3 _playerVelocity;
    private Rigidbody _rb;
    private CapsuleCollider _collider;
    private XRRig _xrRig;
    public float raycastL = 2f;
    //private bool isGrounded => Physics.Raycast(new Vector2(transform.position.x, transform.position.y + 2.0f), Vector3.down, 2.0f); 
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _collider = GetComponent<CapsuleCollider>();
        _xrRig = GetComponent<XRRig>();
        jumpActionRefernceLeft.action.performed += OnJumpLeft;
        jumpActionRefernceRight.action.performed += OnJumpRight;

        
    }

    // Update is called once per frame
    void Update()
    {
        var center = _xrRig.cameraInRigSpacePos;
        _collider.center = new Vector3(center.x, _collider.center.y , center.z);
        _collider.height = _xrRig.cameraInRigSpaceHeight;
        if (Physics.Raycast(new Vector2(transform.position.x, transform.position.y + 2.0f), Vector2.down, raycastL))
        {
            Debug.Log("Grounded");

        }

        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y + 2.0f), Vector2.down, Color.green) ;
    }


    private void OnJumpRight(InputAction.CallbackContext obj)
    {
        Debug.Log("In_Jump");

        if (!Physics.Raycast(new Vector2(transform.position.x, transform.position.y + 2.0f), Vector3.down, raycastL))  return;
        _rb.AddForce(Vector3.up * jumpForce);
       


    }
    private void OnJumpLeft(InputAction.CallbackContext obj)
    {

        Debug.Log("In_Jump");
        if (!Physics.Raycast(new Vector2(transform.position.x, transform.position.y + 2.0f), Vector3.down, raycastL)) return;
        _rb.AddForce(Vector3.up * jumpForce);
        

    }
}

    

