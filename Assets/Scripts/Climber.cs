using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Climber : MonoBehaviour
{
    private XRRig rig;

    private Rigidbody rb;
    private Transform posl;
    public static ControllerVelocity velocityHand;
    public static XRController climbingHand;
    public static bool isOn = false;
    private ActionBasedContinuousMoveProvider continousMovement;
    [Range(0,2)]
    public static int controllerCount = 0;


    // Start is called before the first frame update
    void Start()
    {


        rig = GetComponent<XRRig>();
        continousMovement = GetComponent<ActionBasedContinuousMoveProvider>();
        rb = GetComponent<Rigidbody>();
    }

 
    void FixedUpdate()
    {

        if (controllerCount > 0)
        {
            continousMovement.enabled = false;
            rb.useGravity = false;
            Climb();

        }
        else
        {
            continousMovement.enabled = true;
            rb.useGravity = true;
        }
    }

    void Climb()
    {
        Debug.Log("CLIMB");

        Vector3 velocity = velocityHand ? velocityHand.Velocity : Vector3.zero;
        rb.AddForce(transform.rotation * -velocity);
    }
    }
