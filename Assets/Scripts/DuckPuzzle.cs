using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
public class DuckPuzzle : MonoBehaviour
{
    
    private XRSocketInteractor socket;
    public string TagCheck;
    private GameObject interactable;
    private bool correctTag = false;
    private void Start()
    {
        socket = GetComponent<XRSocketInteractor>();
    }

    public void CheckForTagAdd()
    {
        interactable = socket.selectTarget.gameObject;
        if (interactable.CompareTag(TagCheck))
        {

            DuckCount.DuckCounter += 1;
            correctTag = true;
        }
        else
        {
            correctTag = false;
        }
    }
    public void CheckForTagOut()
    {
        
        if (correctTag)
        {

            DuckCount.DuckCounter -= 1;

        }
    }



}
