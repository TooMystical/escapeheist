using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class DuckCount : MonoBehaviour
{

    public static int DuckCounter = 0;
    public UnityEvent Quack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DuckCounter == 5)
        {
            Debug.Log("Code _ Correct");
            comboCorrect();
        }
    }

    private void comboCorrect()
    {
        
        Quack.Invoke();
        
    }
}
