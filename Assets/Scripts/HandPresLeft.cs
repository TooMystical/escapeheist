using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class HandPresLeft : MonoBehaviour
{
    public InputDeviceCharacteristics controllerCharacteristics;
    public static XRController climbingHand;
    private InputDevice targetDevice;
    // Start is called before the first frame update
    void Start()
    {
        var inputDevices = new List<UnityEngine.XR.InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, inputDevices);

        foreach (var device in inputDevices)
        {
            Debug.Log(string.Format(device.name + device.characteristics));
        }
        if (inputDevices.Count > 0)
        {
            targetDevice = inputDevices[0];
        }


    }

    // Update is called once per frame
    void Update()
    {

        targetDevice.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButtonValue);
        if (primaryButtonValue)
        {
            Debug.Log("Pressing Primary Button");
        }
        targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue);
        if (triggerValue > 0.1f)
        {
            Debug.Log("Trigger Pressed" + triggerValue);
        }
        targetDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 primary2DAxisValue);
        if (primary2DAxisValue != Vector2.zero)
        {
            Debug.Log("JoyStick" + primary2DAxisValue);
        }

    }
}
