using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class RISE : MonoBehaviour
{
    private bool musicplaying = false;
    

    public ActionBasedContinuousMoveProvider continousMovement;

    public bool ducksel = false;
    public Rigidbody rb;
    public AudioSource src;
    // Start is called before the first frame update
    void Start()
    {

    }


    private void Update()
    {
     if (ducksel == true)
        {
            RiseTotheHeavens();
           
        }

    }
    public void Duckheld()
    {
        ducksel = true;
        if (musicplaying == false) { 
        src.Play();
            musicplaying = true;
         }
    }

    void RiseTotheHeavens()
    {
        
        Vector3 velocity = new Vector3(0, 10f, 0);
        rb.AddForce(velocity * Time.deltaTime * 1);
        rb.useGravity = false;
        continousMovement.enabled = false;
    }
}
