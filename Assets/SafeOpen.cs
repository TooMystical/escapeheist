using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeOpen : MonoBehaviour
{
    private bool pressed = false;
    public Animator animator;



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Safe")){
            animator.SetBool("Safe", true);
            pressed = true;
        }
    }


}

